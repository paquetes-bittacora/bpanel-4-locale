<?php

declare(strict_types=1);

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Route;

Route::get(
    '/cambiar-idioma/{locale}',
    static function (string $locale, Request $request, Redirector $redirector): RedirectResponse {
        $request->session()->put('user-locale', $locale);
        return $redirector->route('home');
    }
)->middleware(['web'])->name('bpanel4.change-user-locale');
