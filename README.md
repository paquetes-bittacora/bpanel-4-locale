# bPanel 4 locale

Paquete que se encarga de el cambio de idioma de la web. Registra un middleware y una ruta que se encargan de
establecer el `locale` de la aplicación fácilmente.

Para usarlo, solo hay que poner enlaces en la web para seleccionar el locale, por ejemplo:

```html
<a href="{{ route('bpanel4.change-user-locale', ['locale' => 'es']) }}">Español</a>
<a href="{{ route('bpanel4.change-user-locale', ['locale' => 'en']) }}">Inglés</a>
```

El paquete se instala automáticamente, solo hay que poner los enlaces en la plantilla.

## Lenguaje según dominio

El paquete permite establecer el idioma en función del dominio utilizado para acceder a la página.

En primer lugar habrá que publicar la configuración del paquete:

```
php artisan vendor:publish --provider='Bittacora\Bpanel4\Locale\Bpanel4LocaleServiceProvider'
```

Esto creará el archivo `config/bpanel4-locale.php` donde se configura un array en el que la clave
es el dominio y el valor el locale que debe usarse para ese dominio:

```php
    'domain_locales' => [
        'dominio.es' => 'es',
        'dominio.pt' => 'pt'
    ]
```

Con esta configuración, si accedemos a dominio.es, llegaremos a la página con el locale establecido
a 'es', y si accedemos a dominio.pt, veremos la página con el locale 'pt'.