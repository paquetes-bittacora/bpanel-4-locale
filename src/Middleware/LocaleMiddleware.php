<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Locale\Middleware;

use Closure;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

final class LocaleMiddleware
{
    public function __construct(
        private readonly Application $app,
    ) {
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getLocaleFromSession(Request $request): mixed
    {
        return $request->session()->get('user-locale');
    }

    public function handle(Request $request, Closure $next)
    {
        if (null !== config('bpanel4-locale.domain_locales')) {
            $locale = $this->getLocaleFromDomain($request);
        } else {
            $locale = $this->getLocaleFromSession($request);
        }

        if ($locale) {
            $this->app->setLocale($locale);
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getLocaleFromDomain(Request $request): mixed
    {
        $locale = null;

        $domainLocales = config('bpanel4-locale.domain_locales');
        if (isset($domainLocales[$request->host()])) {
            $locale = $domainLocales[$request->host()];
        }
        return $locale;
    }
}
