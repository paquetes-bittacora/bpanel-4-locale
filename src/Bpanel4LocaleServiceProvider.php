<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Locale;

use Bittacora\Bpanel4\Locale\Commands\RegisterLocaleMiddlewareCommand;
use Illuminate\Support\ServiceProvider;

final class Bpanel4LocaleServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->commands(RegisterLocaleMiddlewareCommand::class);
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->publishes([
            __DIR__.'/../config/bpanel4-locale.php' => config_path('bpanel4-locale.php'),
        ]);
    }
}
